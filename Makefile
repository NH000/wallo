# INSTALL COMMANDS
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# VARIABLES
PROG := wallo
INSTALLDIR := /usr/bin
LOCALEDIR := /usr/share/locale
MANDIR := /usr/share/man

# FUNCTIONS
# =========

# Deletes file $(2) from directory $(1), then deletes empty parent directories subsequently.
delete_file_and_empty_dirs = if test -d '$(1)'; then rm -f '$(1)/$(2)' && rmdir -p --ignore-fail-on-non-empty '$(1)'; fi

# Generates MO file into $(LOCALEDEST) for language code $(1).
generate_mo = mkdir -p '$(LOCALEDIR)/$(1)/LC_MESSAGES' && msgfmt -o '$(LOCALEDIR)/$(1)/LC_MESSAGES/$(PROG).mo' 'po/$(1).po'

# Delete MO file from $(LOCALEDIR) given language code as $(1).
# Then delete all of its parent directories in bottom-to-top order if they are empty.
uninstall_mo = $(call delete_file_and_empty_dirs,$(LOCALEDIR)/$(1)/LC_MESSAGES,$(PROG).mo)

# Recursively installs manual pages to subdirectory $(1) of $(MANDEST).
install_man = $(foreach f,$(notdir $(wildcard man/$(1)/*)),$(if $(shell test -d 'man/$(1)/$(f)' && echo "dir"),$(call install_man,$(1)/$(f)),$(INSTALL_DATA) -D 'man/$(1)/$(f)' '$(MANDIR)/$(1)/$(PROG).1' && sed -i 's/\bWALLO_EXEC_NAME\b/$(PROG)/g' '$(MANDIR)/$(1)/$(PROG).1';))

# Recursively removes installed manual pages from subdirectory $(1) of $(MANDEST).
uninstall_man = $(foreach f,$(notdir $(wildcard man/$(1)/*)),$(if $(shell test -d 'man/$(1)/$(f)' && echo "dir"),$(call uninstall_man,$(1)/$(f)),$(call delete_file_and_empty_dirs,$(MANDIR)/$(1),$(PROG).1);))

# RULES
# =====

.PHONY: help install uninstall

help:
	$(info VARIABLES)
	$(info ================================================================================)
	$(info PROG:       Name of the program as installed.)
	$(info INSTALLDIR: Installation directory.)
	$(info LOCALEDIR:  Locale installation directory.)
	$(info MANDIR:     Manual pages installation directory.)
	$(info )
	$(info RULES)
	$(info ================================================================================)
	$(info help:      Display this help menu.)
	$(info install:   Install the program and its assets.)
	$(info uninstall: Uninstall the program and its assets.)

install: wallo.sh po/*.po man/
	$(INSTALL_PROGRAM) -D wallo.sh '$(INSTALLDIR)/$(PROG)'
	@sed -i "20s/$$/'$(subst /,\/,$(LOCALEDIR))'/;21s/$$/'$(PROG)'/" '$(INSTALLDIR)/$(PROG)'
	$(foreach po,$(wildcard po/*.po),$(call generate_mo,$(patsubst po/%.po,%,$(po)));)
	$(call install_man)

uninstall:
	rm -f '$(INSTALLDIR)/$(PROG)'
	$(foreach po,$(wildcard po/*.po),$(call uninstall_mo,$(patsubst po/%.po,%,$(po)));)
	$(call uninstall_man)
