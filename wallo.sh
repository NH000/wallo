#!/bin/env bash

# Copyright (C) 2019-2021 Nikola Hadžić
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Translation variables.
# These are set during installation.
export TEXTDOMAINDIR=
export TEXTDOMAIN=

# Include translation functions.
source gettext.sh

# Maps given image format variant to its definitive form.
get_definitive_image_format_form()
{
    local format="$(tr '[:upper:]' '[:lower:]' <<< "$1")"

    case "$format" in
        "jpg")
            format="jpeg"
            ;;
    esac 

    echo -n "$format"
}

# Strips away all leading zeroes from resolution and echoes the result.
normalize_resolution()
{
    local -a resolution=()
    IFS="x" read -a resolution <<< "$1"

    local -n width=resolution[0]
    local -n height=resolution[1]

    echo -n "$(expr $width + 0)x$(expr $height + 0)"
}

# Sort entire image array by version of the basenames.
sort_image_array()
{
    ! IFS=$'\n' read -d "" -r -a opt_images <<< "$(printf "%s\n" "${opt_images[@]}" | awk 'BEGIN { FS="/" }; { print $NF, $0 }' | sort -V -k 1 | cut -f2- -d ' ')"
}

# This function modifies image array in this order of steps, in a loop, for all files:
#   1. Converts filename to absolute path, resolving symlinks.
#   2. Removes filename if it contains newline.
#   3. Removes filename if it does not refer to an image (including non-existant files).
#   4. Remove filename if it is already contained in the image array.
# Afterwards the resulting array is sorted by version of the basenames.
setup_image_array()
{
    # Contains indexes of filenames in the image array eligible for excluding.
    # See below for reason why duplicate is not excluded when it is found.
    local -ai to_exclude=()

    local -i i
    for ((i=0;i<${#opt_images[@]};++i)); do
        # Convert filename to absolute path, and resolve it if it is a symlink.
        local filename="${opt_images[$i]}"
        ! opt_images[$i]="$(readlink -qen "${opt_images[$i]}")"

        if [[ "$filename" =~ $'\n' ]]; then # Filename contains newline.
            local -i position=$(($i+1))
            echo "$(eval_gettext "Ignoring \"\$filename\" (\$position): name contains newline.")" 1>&2
            to_exclude+=($i)
        else
            # Get MIME type of the file.
            IFS='/' read -r -a file_type <<< "$(xdg-mime query filetype "$filename" 2>/dev/null)"   # `file_type` is empty on error.

            if [[ ${#file_type[@]} -eq 0 ]]; then   # File's MIME type could not be determined (implied by non-existant files).
                local -i position=$(($i+1))
                echo "$(eval_gettext "Ignoring \"\$filename\" (\$position): cannot determine MIME type.")" 1>&2
                to_exclude+=($i)
            elif [[ "${file_type[0]}" != "image" ]]; then   # File's MIME type is not an image.
                local -i position=$(($i+1))
                echo "$(eval_gettext "Ignoring \"\$filename\" (\$position): not an image.")" 1>&2
                to_exclude+=($i)
            else
                # Checks for duplicate filenames i.e. filenames that refer to the same file.
                # This is done by checking already passed filenames and looking for the same
                # absolute path.
                local -i j
                for ((j=$(($i-1));j>=0;--j)); do
                    if [[ "$filename" == "${opt_images[$j]}" ]]; then   # Duplicate found! Mark it for exclusion.
                        local -i position=$(($i+1))
                        echo "$(eval_gettext "Ignoring \"\$filename\" (\$position): image already specified.")" 1>&2
                        to_exclude+=($i)
                        break
                    fi
                done
            fi
        fi
    done

    # If a filename was marked for exlusion, remove it from the image array. The
    # reason why this is done now (instead of removing duplicate when it is found)
    # is to make subsequent removal faster: if we find another filename that
    # refers to the same file as a previous filename, we will have to search only
    # until the previous duplicate, not until the filename that is considered to
    # be the "original".
    for ((i=0;i<${#to_exclude[@]};++i)); do
        local -i index=$((${to_exclude[$i]}-$i))
        opt_images=("${opt_images[@]::$index}" "${opt_images[@]:$(($index+1))}")
    done

    # Sort image array.
    sort_image_array
}

# Hashes files passed as arguments. Hashes are stored in the `hashes`
# array where each Nth element contains hash of the Nth argument.
# For files for which hashing error occurred, hash at the
# corresponding position will be empty string.
hash_elements()
{
    declare -ga hashes=()

    # Consider all arguments to be files and hash them iteratively.
    # Append every hash to the `hashes` array.
    for filename; do
        IFS=$' \t' read -r -a pair <<< "$("$opt_hash_bin" -b "$filename" 2>/dev/null)"  # `pair` is empty on error.

        if [[ ${#pair[@]} -eq 0 ]]; then
            # On error just append empty string as hash.
            echo "$(eval_gettext "Cannot hash \"\$filename\"!")" 1>&2
            hashes+=("")
        else
            # Add hash of the current file to the hash array.
            hashes+=("${pair[0]}")
        fi
    done
}

# Echoes number suffix of the validly marked basename.
extract_suffix()
{
    : "${1#$WALLO_MARK}"
    echo -n "${_%%.*}"
}

# Echoes extension of the basename (dot included).
extract_extension()
{
    if [[ "$1" =~ "." ]]; then
        echo -n ".${1#*.}"
    fi
}

# Lets `found_index` be the index of the array element that is first found
# to match the specified string.
# If first argument is true, search is performed backward starting from
# the end, and if it is false, search is performed forward starting from the
# beginning. Second argument is the string to match and the rest are array
# elements.
# Returns 1 if there was no match and 0 if there was.
find_value()
{
    local -i i

    if [[ "$1" != "true" ]]; then
        for ((i=3;i<=${#@};++i)); do
            if [[ "$2" == "${!i}" ]]; then
                declare -ig found_index=$(($i-3))
                return 0
            fi
        done
    else
        for ((i=${#@};i>=3;--i)); do
            if [[ "$2" == "${!i}" ]]; then
                declare -ig found_index=$(($i-3))
                return 0
            fi
        done
    fi

    return 1
}

# This function should be used from `fix_invalid_names()`.
# This function will rename specified filename (more on this later) to have the
# number suffix given as `next_suffix`. If a file with target suffix already
# exists, that file, along with files that lie between it and current file is
# renamed so that they all move forward by the appropriate amount to make place
# for the current file without changing the order of the files (this includes
# calling this function recursively).
# Pass index of the reference to the element of `opt_images` array you want to
# fix. Which reference arrays are used depends on the second argument (mode) which
# can be either "valid" or "invalid". In the former case, `valid_suffixes` array
# will be properly updated to match new suffixes of files that are moved. This
# function will update `continue_on_failure` variable, but updating of `next_suffix`
# is left to the caller, since only caller can know whether it wants to increase or
# decrease it and by which amount.
# On failure to move the file, it will be removed from the reference arrays, such
# that it is invisible to subsequent moves. Additionally, if mode is "valid", its
# suffix will be pushed at the end of the `valid_skip` array (unless it already
# exists in that array) to make sure that subsequent moves skip moving to that
# suffix (for file with that suffix already exists, but we don't want it to remain
# in the `valid` and `valid_suffixes` arrays, so that they themselves are not
# moved).
# 0 is returned on success and 1 on error.
fix_invalid_name()
{
    # Determine current and new filename location and name.
    # Index of the reference array element is given as the first argument.
    local -n filename   # Reference variable to the corresponding image array element.
    case "$2" in
        "valid")
            filename=opt_images[${valid[$1]}]
            ;;
        "invalid")
            filename=opt_images[${invalid[$1]}]
            ;;
    esac
    local dirname="$(dirname "$filename")"                          # Directory of the current file.
    local basename="$(basename "$filename")"                        # Basename of the current file.
    local extension="$(extract_extension "$basename")"              # Extension of the current file.
    local new_filename="$dirname/$WALLO_MARK$next_suffix$extension" # Target filename. Identical to the old filename, except for the suffix.

    # If we are moving valid filenames, that means that it is possible that filename
    # with target suffix already exists. In that case attempt to move subsequent
    # files to make space.
    if [[ "$2" == "valid" ]]; then
        # Check whether file with target suffix already exists (among subsequent files).
        # Find the last such file so that all other files with that suffix are also moved
        # (as inbetween files).
        ! find_value true $next_suffix ${valid_suffixes[@]:$(($1+1))}

        # If it does, move it and all files that lie between current and target filename,
        # without changing the order of those files.
        if [[ ${PIPESTATUS[0]} -eq 0 ]]; then
            # Remember location of the target filename, for `found_index` may change
            # when subsequent calls to this function call `find_value`.
            local -ir found_index_copy=$found_index

            # New suffix for the file with target suffix is target suffix plus its distance from the current file.
            next_suffix=$(($next_suffix+1+$found_index_copy))

            local -i i
            # Starting from the target filename, go backward and fix each filename until the current filename
            # is reached. On each iteration target suffix will decrement, so that filename A that is originally
            # behind another filename B, has its new suffix right below suffix new suffix of B. This way the
            # original order of the filenames is not disturbed (unless there were errors when moving files, see
            # `to_skip` below for more details).
            for ((i=$(($1+1+$found_index_copy));i>$1;--i)); do
                # If we reach a suffix that exists in the `valid_skip` array, that is, file with that suffix
                # does exist but could not be moved, skip it. `to_skip` holds the number of such files that
                # need to be skipped to reach next valid target suffix.
                local -i to_skip=0
                while find_value false $next_suffix ${valid_skip[@]}; do
                    to_skip=$(($to_skip+1))
                    next_suffix=$(($next_suffix+1))
                done

                if [[ ${valid_suffixes[$i]} -ne $next_suffix ]]; then   # No reason to move the file if its suffix already matches the target suffix.
                    # Call this function to move current filename to its new target suffix.
                    ! fix_invalid_name $i "valid"

                    # On error, immediately stop moving all files, revert `next_suffix` to original value, and exit with error code.
                    # Note that on error the entire call stack will unfold and we will be back in the `fix_invalid_names()` function.
                    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
                        next_suffix=$(($next_suffix-($i-$1)))
                        return 1
                    fi
                fi

                # Let `next_suffix` point at appropriate suffix for file that comes prior.
                # This may include skipping files that were first forward skipped using `to_skip`,
                # but now they will be backward skipped (so that previous file comes into the
                # position occupid by the just moved file).
                next_suffix=$(($next_suffix-1-$to_skip))
            done
        fi
    fi

    # Attempt to rename the current file so that it contains the next suffix.
    echo "\"$filename\" -> \"$new_filename\""
    ! rename -o "$filename" "$new_filename" "$filename" 2>/dev/null

    # On success, update entry in the image array.
    if [[ ${PIPESTATUS[0]} -eq 0 ]]; then
        # If we are moving valid filenames, also update suffix of the current file.
        if [[ "$2" == "valid" ]]; then
            valid_suffixes[$1]=$next_suffix
        fi

        # Update `opt_images` entry so that it contains the new filename.
        filename="$new_filename"
    else
        # On error, display error message to the user and prompt him for what to do next. Default action is to exit, so that subsequent moves don't scramble the order of filenames.

        echo "$(gettext "Renaming failed!")"

        if [[ "$opt_no_confirm" == true ]]; then
            exit $exit_running
        elif [[ "$continue_on_failure" != true ]]; then
            shopt -s nocasematch

            while :; do
                read -r -p "$(gettext "Continue? (yes/No/always) ")" 2>&1

                if [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "yes")"[[:blank:]]*$ ]]; then
                    break
                elif [[ "$REPLY" =~ ^[[:blank:]]*("$(gettext "no")")?[[:blank:]]*$ ]]; then
                    exit $exit_running
                elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "always")"[[:blank:]]*$ ]]; then
                    continue_on_failure=true
                    break
                fi
            done

            shopt -u nocasematch
        fi

        # Remove filename whose moving filename from the reference arrays, so that it is
        # not attempted to move them again.
        case "$2" in
            "valid")
                # For valid filenames, there is also possibility that it will be tried to move
                # other filenames to suffix of the file that failed to move (which already exists).
                # Thus, if that suffix is not already marked for skipping, add it to the skip array.
                if ! find_value true ${valid_suffixes[$1]} ${valid_skip[@]}; then
                    valid_skip+=(${valid_suffixes[$1]})
                fi

                valid=(${valid[@]::$1} ${valid[@]:$(($1+1))})
                valid_suffixes=(${valid_suffixes[@]::$1} ${valid_suffixes[@]:$(($1+1))})
                ;;
            "invalid")
                invalid=(${invalid[@]::$1} ${invalid[@]:$(($1+1))})
                ;;
        esac

        return 1
    fi
}

# Removes duplicate images from the image array and deletes them from the filesystem.
# This function uses hashes to determine whether an image is a duplicate, thus `hashes`
# global variable must contain hashes corresponding to elements in the `opt_images` array.
remove_duplicates()
{
    local continue_on_failure=$opt_no_confirm   # If true, always continue removing duplicates when removing a duplicate fails.
    local -a to_exclude=()                      # Indexes of duplicate images to be excluded after removal. See below for reason why duplicate is not excluded when its corresponding file is deleted.

    local -i i
    # For each element, go backward and check whether an identical hash exists.
    # If it does, remove corresponding file on the disk, and memorise index of the file
    # to be excluded later from the image array.
    for ((i=0;i<${#hashes[@]};++i)); do
        # Skip unhashed files.
        if [[ -z "${hashes[$i]}" ]]; then
            continue
        fi

        local -i j
        for ((j=$(($i-1));j>=0;--j)); do
            if [[ "${hashes[$j]}" == "${hashes[$i]}" ]]; then
                local original="${opt_images[$j]}"
                local duplicate="${opt_images[$i]}"
                echo "$(eval_gettext "\"\$duplicate\" -> \"\$original\": deleting duplicate...")"

                ! rm -f "$duplicate" 2>/dev/null
                if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
                    echo "$(gettext "Deletion failed!")"

                    if [[ "$continue_on_failure" != true ]]; then
                        shopt -s nocasematch

                        while :; do
                            read -p "$(gettext "Continue? (Yes/no/always) ")" -r 2>&1

                            if [[ "$REPLY" =~ ^[[:blank:]]*("$(gettext "yes")")?[[:blank:]]*$ ]]; then
                                break
                            elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "no")"[[:blank:]]*$ ]]; then
                                exit $exit_running
                            elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "always")"[[:blank:]]*$ ]]; then
                                continue_on_failure=true
                                break
                            fi
                        done

                        shopt -u nocasematch
                    fi
                else
                    # Add current index to the indexes to be exclude later, and let duplicate index point to original image.
                    # The reason why this is done (instead of removing duplicate from both hash and image arrays) is to make
                    # subsequent removal faster: if we find another image with the same hash, we will have to search only
                    # until the previous duplicate, not until the original image.
                    to_exclude+=($i)
                    opt_images[$i]="$original"
                fi

                # Duplicate found, stop searching.
                break
            fi
        done
    done

    # Exclude found duplicates from the image and hash arrays.
    for ((i=0;i<${#to_exclude[@]};++i)); do
        local -i index=$((${to_exclude[$i]}-$i))
        opt_images=("${opt_images[@]::$index}" "${opt_images[@]:$(($index+1))}")
    done
}

# This function "fixes invalid filenames" by renaming invalidly marked files
# and collapsing validly marked ranges.
#   1. Collects references to invalidly marked files, and creates ranges of validly marked files.
#   2. Collapses validly marked file ranges into one range, starting from the first range.
#   Array is also optionally moved forward/backward, to match the starting position.
#   4. Appends invalidly marked files to the end of the validly marked array, renaming them to be valid.
# This function expects image array to be sorted by version in respect to their basenames.
fix_invalid_names()
{
    local continue_on_failure=false # If true, operation will continue on failure automatically, rather than prompting the user.

    local -ai invalid=()        # Array containing indexes of invalidly marked files.
    local -ai valid=()          # Array containing indexes of validly marked files.
    local -ai valid_suffixes=() # Array containing suffixes of validly marked files. Each Nth element is suffix of filename whose index is the Nth element of `valid` array.
    local -ai valid_skip=()     # Array containing suffixes which can't be moved and should be skipped.

    # Collects indexes of invalidly and validly marked filenames into arrays.
    # Suffixes for valid filenames are also gathered.
    local -i i
    for ((i=0;i<${#opt_images[@]};++i)); do
        local basename="$(basename "${opt_images[$i]}")"

        if [[ "$basename" =~ ^"$WALLO_MARK" ]]; then    # File is marked.
            if [[ ! "$basename" =~ ^"$WALLO_MARK"[[:digit:]]+(\..*)?$ ]]; then  # Save index of invalidly marked filename to `invalid` array.
                invalid+=($i)
            else    # Save index of validly marked filename and its suffix to `valid` and `valid_suffixes` arrays, respectively.
                valid+=($i)
                valid_suffixes+=($(extract_suffix "$basename"))
            fi
        fi
    done

    # Suffix the next filename in the array of validly marked files should have.
    # Used to determine whether file should be moved or not.
    local -i next_suffix
    if [[ "$opt_zero" != true ]]; then
        next_suffix=1
    else
        next_suffix=0
    fi

    # Collapse all validly defined ranges into one, and move the entire range forwards/backwards
    # to place it into the proper starting position.

    echo "$(gettext "Collapsing validly marked images...")"

    # Starting from the beginning of `valid` array, check whether the suffix of the current file
    # matches the next predicted suffix. If not, rename the file to match it (along with all files
    # that are inbetween and up to files that come later but have the next prediceted suffix, such
    # that original order of files is not disturbed).
    for ((i=0;i<${#valid[@]};++i)); do
        # Step over all suffixes that belong to files that can't be moved.
        # This has the effect of skipping already existing files whose moving failed.
        while find_value false $next_suffix ${valid_skip[@]}; do
            valid_skip=(${valid_skip[@]::$found_index} ${valid_skip[@]:$(($found_index+1))})
            next_suffix=$(($next_suffix+1))
        done

        if [[ ${valid_suffixes[$i]} -ne $next_suffix ]]; then   # Step over files whose suffix already matches next predicted suffix.
            # Attempt to fix the filename.
            # Note that `valid_skip` will always be empty if this call results in range moves.
            ! fix_invalid_name $i "valid"

            # On failure, try moving to the same suffix either the same file or the next one.
            #   - If some subsequent file failed to move, decrementing `i` will have the effect of
            #   choosing the same file again.
            #   - If the current file itself failed to move, since it was removed from the `valid`
            #   and `valid_suffixes` arrays, decrementing `i` will have the effect of choosing
            #   the file that comes immediately after it (because it is now in its place).
            # Also note that in the former case, file with whichever suffix failed to move, that
            # suffix will be skipped using `valid_skip` array.
            if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
                next_suffix=$(($next_suffix-1))
                i=$(($i-1))
            fi
        fi

        next_suffix=$(($next_suffix+1))
    done

    unset -v valid valid_suffixes valid_skip # Not needed anymore.

    # Push invalidly marked filenames at the end of the validly marked filenames.

    echo "$(gettext "Appending invalidly marked images...")"

    # Scanning the array of invalid files, try to move each one to the next predicted suffix.
    # Next predicted suffix is inherited from the previous loop, which means that it will
    # contain suffix one plus last existing suffix.
    for ((i=0;i<${#invalid[@]};++i)); do
        ! fix_invalid_name $i "invalid"

        # Move next suffix pointer only when the current one has been filled.
        # Otherwise, decrement `i` to point at the next file (which now occupies
        # position occupied by file whose moving was attempted in this iteration).
        if [[ ${PIPESTATUS[0]} -eq 0 ]]; then
            next_suffix=$(($next_suffix+1))
        else
            i=$(($i-1))
        fi
    done

    unset -v invalid    # Not needed anymore.
}

# Finds the highest suffix among validly marked filenames, and renames unmarked
# files such that they are validly marked files whose suffixes begin right after
# the found highest suffix. `opt_images` array is also updated.
# This function expects `opt_images` to be sorted by version of the basenames.
organize_images()
{
    # Find the highest suffix among validly marked files, and collect indexes unmarked files into `unmarked` array.
    local -ia unmarked=()
    local -i i
    for ((i=0;i<${#opt_images[@]};++i)); do
        local basename="$(basename "${opt_images[$i]}")"

        if [[ "$basename" =~ ^"$WALLO_MARK"[[:digit:]]+(\..*)?$ ]]; then
            local -i next_suffix=$(($(extract_suffix "$basename")+1))
        elif [[ ! "$basename" =~ ^"$WALLO_MARK" ]]; then
            unmarked+=($i)
        fi
    done

    # Initial value, if there were no validly marked filenames.
    if [[ ! -v next_suffix ]]; then
        local -i next_suffix

        if [[ "$opt_zero" == true ]]; then
            next_suffix=0
        else
            next_suffix=1
        fi
    fi

    local continue_on_failure=$opt_no_confirm   # If true, continue renaming files on error without prompting the user.

    for ((i=0;i<${#unmarked[@]};++i)); do
        # Determine new filename.
        local -n filename=opt_images[${unmarked[$i]}]
        local dirname="$(dirname "$filename")"
        local basename="$(basename "$filename")"
        local extension="$(extract_extension "$basename")"
        local new_filename="$dirname/$WALLO_MARK$next_suffix$extension"

        # Attempt to move file to the new filename.
        echo "\"$filename\" -> \"$new_filename\""
        ! rename -o "$filename" "$new_filename" "$filename" 2>/dev/null

        if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
            echo "$(gettext "Renaming failed!")"

            if [[ "$continue_on_failure" != true ]]; then
                shopt -s nocasematch

                while :; do
                    read -r -p "$(gettext "Continue? (Yes/no/always) ")" 2>&1

                    if [[ "$REPLY" =~ ^[[:blank:]]*("$(gettext "yes")")?[[:blank:]]*$ ]]; then
                        break
                    elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "no")"[[:blank:]]*$ ]]; then
                        exit $exit_running
                    elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "always")"[[:blank:]]*$ ]]; then
                        continue_on_failure=true
                        break
                    fi
                done

                shopt -u nocasematch
            fi
        else
            filename="$new_filename"
        fi

        next_suffix=$(($next_suffix+1))
    done
}

# Converts all images in the image array to format `opt_convert`.
# If `opt_keep_extension` is true, name of the file is unmodified.
# Otherwise, extension of the file is replaced with extension of the new format.
convert_images()
{
    local continue_on_failure=$opt_no_confirm   # If true, instead of prompting the user what to do, error is just skipped.

    local -i i
    for ((i=0;i<${#opt_images[@]};++i)); do
        local image="${opt_images[$i]}"

        IFS='/' read -r -a file_type <<< "$(xdg-mime query filetype "$image" 2>/dev/null)"  # `file_type` is empty on error.

        if [[ "${file_type[0]}" != "image" ]]; then
            echo "$(eval_gettext "\"\$image\" is not an image! Skipping...")" 1>&2
            continue
        fi

        # Skip images that are already of desired format.
        if [[ "${file_type[1]}" == "$opt_convert" ]]; then
            continue
        fi

        local new_image="$image"

        if [[ "$opt_keep_extension" != true ]]; then
            local extension="$(extract_extension "$(basename "$new_image")")"
            new_image="${new_image/%"$extension"/".$opt_convert"}"
        fi

        local current_format="${file_type[1]}"

        echo "$(eval_gettext "Converting format of \"\$image\": '\$current_format' -> '\$opt_convert'")"
        ! ([[ "$image" == "$new_image" || ! -a "$new_image" ]] && magick convert "$image" "$opt_convert:$new_image" 2>/dev/null)  # If current file itself is not being overwritten, check whether file already exists.

        if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
            echo "$(gettext "Conversion failed!")"

            if [[ "$continue_on_failure" != true ]]; then
                shopt -s nocasematch

                while :; do
                    read -r -p "$(gettext "Continue? (Yes/no/always) ")" 2>&1

                    if [[ "$REPLY" =~ ^[[:blank:]]*("$(gettext "yes")")?[[:blank:]]*$ ]]; then
                        break
                    elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "no")"[[:blank:]]*$ ]]; then
                        exit $exit_running
                    elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "always")"[[:blank:]]*$ ]]; then
                        continue_on_failure=true
                        break
                    fi
                done

                shopt -u nocasematch
            fi
        elif [[ "$image" != "$new_image" ]]; then
            ! rm -f "$image"
            opt_images[$i]="$new_image"
        fi
    done
}

# Resizes all images in the image array to resolution `opt_resolution`.
resize_images()
{
    local continue_on_failure=$opt_no_confirm   # If true, continue on error instead of prompting user what to do.

    for image in "${opt_images[@]}"; do
        local current_resolution="$(magick identify -format "%wx%h" "$image" 2>/dev/null)"
        : "${current_resolution:="???"}"

        # Skip images that are already of desired resolution.
        if [[ "$current_resolution" == "$opt_resolution" ]]; then
            continue
        fi

        echo "$(eval_gettext "Resizing \"\$image\": \$current_resolution -> \$opt_resolution...")"
        ! magick mogrify -resize "$opt_resolution$opt_resize_mode" "$image" 2>/dev/null

        if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
            echo "$(gettext "Resizing failed!")"

            if [[ "$continue_on_failure" != true ]]; then
                shopt -s nocasematch

                while :; do
                    read -r -p "$(gettext "Continue? (Yes/no/always) ")" 2>&1

                    if [[ "$REPLY" =~ ^[[:blank:]]*("$(gettext "yes")")?[[:blank:]]*$ ]]; then
                        break
                    elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "no")"[[:blank:]]*$ ]]; then
                        exit $exit_running
                    elif [[ "$REPLY" =~ ^[[:blank:]]*"$(gettext "always")"[[:blank:]]*$ ]]; then
                        continue_on_failure=true
                        break
                    fi
                done

                shopt -u nocasematch
            fi
        fi
    done
}

# Displays help menu.
help_menu()
{
    echo "wallo - $(gettext "wallpaper organizer")"
    echo
    echo "$(gettext "Version"): 1.0"
    echo "$(gettext "License"): GPL3"
    echo
    echo "$(gettext "USAGE"):"
    echo -e "$0 [-c $(gettext "FORMAT")] [-r $(gettext "WIDTHxHEIGHT")] [-z] [-n] [-f] [-d] [--keep-extension] [--resize-mode $(gettext "MODE")] [--hash-bin $(gettext "PROGRAM")] [--no-confirm] [-- $(gettext "FILE")\u2026]"
    echo
    echo "$(gettext "If run with no arguments, this program displays help menu and exits.")"
    echo
    echo "$(gettext "OPTIONS"):"
    echo -e "-c $(gettext "FORMAT"), --convert $(gettext "FORMAT")\n\t$(gettext "convert images to format FORMAT")"
    echo -e "-r $(gettext "WIDTHxHEIGHT"), --resize $(gettext "WIDTHxHEIGHT")\n\t$(gettext "resize images to resolution WIDTHxHEIGHT")"
    echo -e "-z, --zero\n\t$(gettext "suffix of marked images starts from 0 instead of 1")"
    echo -e "-n, --no-organize\n\t$(gettext "do not organize images")"
    echo -e "-f, --fix-invalid\n\t$(gettext "collapse validly marked and append invalidly marked images")"
    echo -e "-d, --remove-duplicates\n\t$(gettext "find duplicates and remove them")"
    echo -e "--keep-extension\n\t$(gettext "do not change file extension when converting to another format")"
    echo -e "--resize-mode $(gettext "MODE")\n\t$(gettext "resize mode")"
    echo -e "--hash-bin $(gettext "PROGRAM")\n\t$(gettext "program to use for hashing")"
    echo -e "--no-confirm\n\t$(gettext "automatically answer with default answer to every prompt")"
    echo
    echo "$(gettext "POSITIONAL PARAMETERS"):"
    echo -e "$(gettext "FILE")\n\t$(gettext "file to act upon, must be an image")"
    echo
    echo "$(gettext "ENVIRONMENT VARIABLES"):"
    echo -e "WALLO_MARK\n\t$(gettext "prefix that designates organized images")"
}

# Sets unset environment variables to default values, and exits
# with error code if already set environment variables are invalid.
test_env()
{
    # Set unset variables to default values.
    : "${WALLO_MARK="wallpaper"}"

    # Export all used environment variables just in case they are not exported.
    export WALLO_MARK

    # Test whether variables associated with filenames contain newline.
    for var in "WALLO_MARK"; do
        if [[ "${!var}" =~ $'\n' ]]; then
            echo "$(eval_gettext "Environment variable \"\$var\" contains newline.")" 1>&2
            exit $exit_environment
        fi
    done
}

# Parses command-line arguments and sets option values.
# Pass entire argument vector to this function.
parse_args()
{
    if [[ $# -eq 0 ]]; then
        help_menu
        exit $exit_success
    else
        until [[ $# -eq 0 || "$1" == "--" ]]; do
            case "$1" in
                "-c"|"--convert")
                    if [[ $# -eq 1 ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                        exit $exit_environment
                    fi

                    shift
                    opt_convert="$(get_definitive_image_format_form "$1")"
                    ;;
                "-r"|"--resolution")
                    if [[ $# -eq 1 ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                        exit $exit_environment
                    elif [[ ! "$2" =~ 0*[1-9][[:digit:]]*"x"0*[1-9][[:digit:]]*  ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: invalid value for option -- '\$option'")" 1>&2
                        exit $exit_environment
                    fi

                    shift
                    opt_resolution="$(normalize_resolution "$1")"
                    ;;
                "-z"|"--zero")
                    opt_zero=true
                    ;;
                "-n"|"--no-organize")
                    opt_no_organize=true
                    ;;
                "-f"|"--fix-invalid")
                    opt_fix_invalid=true
                    ;;
                "-d"|"--remove-duplicates")
                    opt_remove_duplicates=true
                    ;;
                "--keep-extension")
                    opt_keep_extension=true
                    ;;
                "--resize-mode")
                    if [[ $# -eq 1 ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                        exit $exit_environment
                    elif [[ "$2" != "^" && "$2" != "!" && "$2" != ">" && "$2" != "<" ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: invalid value for option -- '\$option'")" 1>&2
                        exit $exit_environment
                    fi

                    shift
                    opt_resize_mode="$1"
                    ;;
                "--hash-bin")
                    if [[ $# -eq 1 ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: option needs value -- '\$option'")" 1>&2
                        exit $exit_environment
                    elif [[ "$2" != "md5sum" && "$2" != "sha224sum" && "$2" != "sha256sum" && "$2" != "sha384sum" && "$2" != "sha512sum" && "$2" != "b2sum" ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: invalid value for option -- '\$option'")" 1>&2
                        exit $exit_environment
                    fi

                    shift
                    opt_hash_bin="$1"
                    ;;
                "--no-confirm")
                    opt_no_confirm=true
                    ;;
                *)
                    if [[ "${1::1}" != "-" || ${#1} -eq 1 ]]; then
                        local -r prog_name="$0"
                        local -r option="$1"
                        echo "$(eval_gettext "\$prog_name: unrecognized option -- '\$option'")" 1>&2
                        exit $exit_environment
                    else
                        local -i i
                        for ((i=1;i<${#1};++i)); do
                            case "${1:$i:1}" in
                                "z")
                                    opt_zero=true
                                    ;;
                                "n")
                                    opt_no_organize=true
                                    ;;
                                "f")
                                    opt_fix_invalid=true
                                    ;;
                                "d")
                                    opt_remove_duplicates=true
                                    ;;
                                *)
                                    local -r prog_name="$0"
                                    local -r option="$1"
                                    echo "$(eval_gettext "\$prog_name: unrecognized option -- '\$option'")" 1>&2
                                    exit $exit_environment
                                    ;;
                            esac
                        done
                    fi
            esac

            shift
        done
    fi

    # Collect position arguments into image array.
    if [[ $# -le 1 ]]; then
        local -r prog_name="$0"
        echo "$(eval_gettext "\$prog_name: files to act upon not supplied")" 1>&2
        exit $exit_environment
    else
        shift

        while [[ $# -gt 0 ]]; do
            opt_images+=("$1")
            shift
        done
    fi
}

# Exit status.
declare -gir exit_success=0     # No errors.
declare -gir exit_environment=1 # Bad environment or command-line.
declare -gir exit_running=2     # Error during execution. 

# Trap signals.
trap 'exit $exit_environment' SIGTERM
trap 'exit $exit_running' SIGINT

# Prepare shell environment.
set -o errexit -o pipefail -o nounset -o noglob
shopt -u nocasematch

# Program options, set to default values.
declare -g opt_convert=""
declare -g opt_resolution=""
declare -g opt_zero=false
declare -g opt_no_organize=false
declare -g opt_fix_invalid=false
declare -g opt_remove_duplicates=false
declare -g opt_keep_extension=false
declare -g opt_resize_mode=0
declare -g opt_hash_bin="md5sum"
declare -g opt_no_confirm=false
declare -ga opt_images=()

# Initialize/check validity of the environment.
test_env

# Parse command-line arguments.
parse_args "$@"

# Prepare image array.
setup_image_array

# Operation stages:
#   1. Removing duplicate images.
#   2. Fixing invalid filenames.
#   3. Organizing unmarked images.
#   4. Changing format of the images.
#   5. Resizing images.

# Remove duplicates.
if [[ "$opt_remove_duplicates" == true ]]; then
    echo "===$(gettext "DUPLICATE REMOVAL")==="

    hash_elements "${opt_images[@]}"
    remove_duplicates
    unset -v hashes # Hashes are not needed anymore.
fi

# Fix invalid filenames.
if [[ "$opt_fix_invalid" == true ]]; then
    echo "===$(gettext "INVALID NAMES FIX")==="

    fix_invalid_names

    # Array was modified, sort it again.
    sort_image_array
fi

# Organize unmarked images.
if [[ "$opt_no_organize" != true ]]; then
    echo "===$(gettext "ORGANIZING")==="

    organize_images

    # Array was modified, sort it again.
    sort_image_array
fi

# Convert images to specified format.
if [[ -n "$opt_convert" ]]; then
    echo "===$(gettext "FORMAT CONVERSION")==="

    convert_images

    # Array was modified, sort it again.
    sort_image_array
fi

# Resize images to specified resolution.
if [[ -n "$opt_resolution" ]]; then
    echo "===$(gettext "RESIZING")==="

    resize_images
fi
