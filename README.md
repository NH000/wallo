# wallo

## Description
wallo is a *wallpaper organizer*.
That of course does not mean that it can be used only on wallpapers; it can be used on any kind of image.

What wallo can do is:

+ rename selected images such that their names have the same prefix ("mark") along with unique image number
+ convert images type
+ resize images
+ fix invalid (in respect to this program) filenames
+ remove duplicates

It's something that I wrote to organize my own wallpaper collection, hence the name.

## Requirements

#### For installation
+ make
+ sh
+ msgfmt
+ coreutils
+ sed

#### For running
+ bash
+ gettext
+ coreutils
+ util-linux
+ awk
+ xdg-utils
+ magick (from ImageMagick)

## Installation
To install this utility, go into the project's root directory and run `make install`.
You can also configure installation parameters, run `make help` to see them.

## Uninstallation
Similar as for the installation, go into the project's root directory and run `make uninstall`.
Make sure that make variables are set to the values that were used during the installation, because they determine installation directory, executable name, etc.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions**                |
|--------------------|----------------------------------------------------------|---------------------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0                             |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0                             |

### Translation process
This program is written to be easily translatable to multiple languages, and it achieves that through the use of [`gettext`](https://www.gnu.org/software/gettext/) library.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

To add a new or update existing translation, enter the project's `po` directory and run the following command:

```
make %.po  # Generate/update PO file; "%" should be replaced with a language code.
```

Afterwards, you can edit created/updated PO file (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details),
translating the program that way.

You could also run `make messages.pot` to just generate the template file, but this will be done automatically by the previously described rule.

Also, it would be good to translate the manual page; you will find it in `man` project subdirectory.
